from typing import List

import numpy as np
from unittest import TestCase

from keras import Sequential
from keras.layers import Dense

from src.network.network import NeuronNetwork
from src.network.network_visualizer import NetworkVisualizer


class TestNetwork(TestCase):
    def test_forward_act_one_out(self):
        network = NeuronNetwork()
        network.add_layer(w=np.array([[2, 2, 2], [2, 2, 2]], dtype=float),
                          b=None)
        network.add_layer(w=np.array([[1, 1]], dtype=float), b=None)
        x = np.array([0, 1, 2])
        ys = network.get_net_activation(x)
        self.assertEqual(1, len(ys))
        delta = abs(0.88027688 - ys[0])
        self.assertLess(delta, 0.0001)

    def test_forward_act_two_outs(self):
        network = NeuronNetwork()
        network.add_layers([10, 5, 3, 3])
        x = np.array([0.5] * 10)
        ys = network.get_net_activation(x)
        self.assertEqual(3, len(ys))
        self.assertGreater(ys[0], 0)
        self.assertGreater(ys[1], 0)
        self.assertLess(ys[0], 1)
        self.assertLess(ys[1], 1)

    def test_train_batch(self):
        x_batch = [[1, 2, 0.5], [0.8, 0.2, 0.6], [0.9, 0.2, 0.7]]
        y_batch = [[1, 1], [0, 0], [0, 1]]
        network = NeuronNetwork(learning_rate=1)
        network.add_layers([3, 2, 2])
        network.train_batch(x_batch, y_batch)

    def test_train(self):
        np.random.seed(42)
        inp_len = 10
        set_len = 500
        x = np.random.randn(set_len, inp_len)
        y = []
        for i in range(set_len):
            yi = complex_margin_func(x[i])
            y.append(yi)

        network = NeuronNetwork(learning_rate=0.5)
        network.l1 = 0.0001
        network.l2 = 0.0001
        network.add_layers([10, 10, 5, 5, 4, 4])
        network.train(x, y, max_gens=1000)
        #self.assertLess(network.error_on_trainset[-1],
        #                network.error_on_trainset[0])
        vs = NetworkVisualizer()
        vs.build_learning_curve(network)

    def test_keras(self):
        np.random.seed(42)
        inp_len = 10
        set_len = 500
        x = np.random.randn(set_len, inp_len)
        y = []
        for i in range(set_len):
            yi = complex_margin_func(x[i])
            y.append(yi)
        x = np.array(x)
        y = np.array(y)

        model = Sequential()
        #model.add(Dense(10, activation='sigmoid'))
        model.add(Dense(10, input_dim=10, activation='sigmoid'))
        model.add(Dense(5, activation='sigmoid'))
        model.add(Dense(5, activation='sigmoid'))
        model.add(Dense(4, activation='sigmoid'))
        model.add(Dense(4, activation='sigmoid'))
        model.compile(loss="binary_crossentropy",
                      optimizer="adam",
                      metrics=['accuracy'])
        model.fit(x, y, epochs=1000, batch_size=10)
        scores = model.evaluate(x, y)
        print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))
        self.assertLess(scores[1], 100)

        #sgd = optimizers.SGD(lr=0.5, decay=1e-6, momentum=0.9, nesterov=True)
        #model.compile(loss='mean_squared_error', optimizer=sgd)

    def test_nabla_w(self):
        nn = NeuronNetwork()
        nn.add_layers([3, 2])
        nn.layers[0].b = [-1.0, -1]
        nn.layers[0].w = np.array([[-1.0, 1, -1], [1, -1, 1]])
        old_w = np.copy(nn.layers[0].w)

        x = [1.0, 2, 3]
        y = [0, 1.0]
        nn.get_net_activation(x)
        nn.calc_out_error(y)
        nn.recalc_weights(1)

        nabla_w = old_w - nn.layers[0].w
        true_w = np.array([[ 0.00214254, 0.00428509, 0.00642763],
                           [-0.05287709, -0.10575419, -0.15863128]])
        delta_nabla = true_w - nabla_w
        for r in range(nabla_w.shape[0]):
            for c in range(nabla_w.shape[1]):
                self.assertLess(abs(delta_nabla[r][c]), 0.0001)


def complex_margin_func(x) -> List[float]:
    y0 = 1 if x[0] + x[1] > 0 else 0
    y1 = 1 if x[2] - x[4] > 0 else 0
    y2 = 1 if x[5] - x[6] > 0 else 0
    y3 = 1 if x[8] > 2 * x[9] else 0
    return [y0, y1, y2, y3]


def non_complex_margin_func(x) -> List[float]:
    y0 = 1 if x[0] * x[1] - 2 * x[2] > 0 else 0
    y1 = 1 if x[2] * x[3] - x[4] > 0 else 0
    y2 = 1 if x[5] * x[7] - x[6] > 0 else 0
    y3 = 1 if x[8] > 2 * x[9] else 0
    return [y0, y1, y2, y3]
