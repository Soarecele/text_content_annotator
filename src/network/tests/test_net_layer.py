from unittest import TestCase

import numpy as np

from src.network.net_layer import NeuronLayer
from src.network.network import NeuronNetwork


class TestNetwork(TestCase):
    def test_sum(self):
        layer = NeuronLayer(1,
                            w=np.array([[2, 3], [3, 2], [1, 2]], dtype=float),
                            sigm=NeuronNetwork.default_sigm,
                            sprime=NeuronNetwork.default_sprime,
                            b = None)
        sm = layer.get_sum([1, 0])
        self.assertEqual(2.0, sm[0])
        self.assertEqual(3.0, sm[1])
        self.assertEqual(1.0, sm[2])

    def test_activation(self):
        layer = NeuronLayer(1,
                            w=np.array([[2, 3], [3, 2], [1, 2]], dtype=float),
                            sigm=NeuronNetwork.default_sigm,
                            sprime=NeuronNetwork.default_sprime,
                            b=None)
        act = layer.get_activation([1, 0])
        self.assertGreater(act[0], 0.88)
        self.assertGreater(act[1], 0.95)
        self.assertGreater(act[2], 0.73)

    def test_out_error(self):
        layer = NeuronLayer(1,
                            w=np.array([[2, 3], [3, 2], [1, 2]], dtype=float),
                            sigm=NeuronNetwork.default_sigm,
                            sprime=NeuronNetwork.default_sprime,
                            b=None)
        layer.get_activation([1, 0])
        err = layer.calc_out_error([1, 1, 1])
        self.assertEqual(3, len(err))
        for e in err:
            self.assertLess(e, 0)

    def test_backward_error(self):
        next_layer = NeuronLayer(1,
                            w=np.array([[1, 0.75, 0.8], [0.5, 1.5, 1.2]], dtype=float),
                            sigm=NeuronNetwork.default_sigm,
                            sprime=NeuronNetwork.default_sprime,
                            b=None)
        next_layer.get_activation([0.7, 0.5, 1])
        next_layer.calc_out_error([1, 0])

        layer = NeuronLayer(1,
                            w=np.array([[2, 3], [3, 2], [1, 2]], dtype=float),
                            sigm=NeuronNetwork.default_sigm,
                            sprime=NeuronNetwork.default_sprime,
                            b=None)
        layer.get_activation([0.75, 0.5])

        err = layer.backward_error(next_layer)
        self.assertEqual(3, len(err))
        for e in err:
            self.assertGreater(e, 0)

    def test_recalc_weights(self):
        layer = NeuronLayer(1,
                            w=np.array([[2, 3], [3, 2], [1, 2]], dtype=float),
                            sigm=NeuronNetwork.default_sigm,
                            sprime=NeuronNetwork.default_sprime,
                            b=None)
        layer.get_activation([1, 0])
        layer.calc_out_error([1, 1, 1])
        layer.recalc_weights(1, [0.87, 0.12])
