from collections import Callable

import numpy as np


class NeuronLayer:
    def __init__(self,
                 level: int,
                 w,
                 sigm: Callable,
                 sprime: Callable,
                 b=None,
                 l1: float = 0,
                 l2: float = 0):
        # w(nl, nl-1)
        # b(nl)
        # regularization coeffs
        self.l1 = l1
        self.l2 = l2
        self.sigm = sigm
        self.sprime = sprime
        self.level = level
        self.w = w
        self.b = b if b is not None else [0.0] * w.shape[0]
        self.z = []  # summatory
        self.a = 0.0  # activation
        self.error = np.array([0.0] * w.shape[0]) # (nl)

    def get_sum(self, x):  # z(nl, 1)
        # x(nl-1), w(nl, nl-1), b(nl)
        self.z = self.w.dot(x) + self.b  # (nl)
        return self.z

    def get_activation(self, x):  # a(nl)
        z = self.get_sum(x)
        self.a = self.sigm(z)
        return self.a

    def calc_out_error(self, y): # nablaJ(nl)
        sprm = self.sprime(self.z) # self.a * (1 - self.a)
        self.error += (self.a - y) * sprm
        return self.error

    def backward_error(self, next_layer):
        dj = next_layer.w.T.dot(next_layer.error)
        spr = self.sprime(self.z) # self.a * (1 - self.a)
        self.error += dj * spr
        return self.error

    def recalc_weights(self, learning_rate: float, prev_a):
        # prev_a(n_l)
        self.b -= self.error * learning_rate
        dw = np.matrix(prev_a).T.dot(np.matrix(self.error)) * learning_rate
        dw = dw.T + self.l1 * np.sign(self.w) + self.l2 * self.w
        self.w -= dw
