import numpy as np
import random
from typing import List, Callable

from src.network.net_functions import sigmoid, sigmoid_prime, error_fn_distance
from src.network.net_layer import NeuronLayer


class NeuronNetwork:
    default_sigm = np.vectorize(sigmoid)
    default_sprime = np.vectorize(sigmoid_prime)

    def __init__(self,
                 sigm: Callable = None,
                 sprime: Callable = None,
                 error_func: Callable = None,
                 learning_rate: float = 1):
        self.layers = []  # type:List[NeuronLayer]
        self.x = None
        self.sigm = sigm or self.default_sigm
        self.sprime = sprime or self.default_sprime
        self.error_func = error_func or error_fn_distance
        self.learning_rate = learning_rate
        # regularization coeffs
        self.l1 = 0.0
        self.l2 = 0.0
        # error by generation
        self.error_on_trainset = []  # type: List[float]

    def add_layers(self, layers: List[int]):
        for i in range(1, len(layers)):
            in_ct = layers[i - 1]
            neurons = layers[i]
            w = np.random.randn(neurons, in_ct)
            self.add_layer(w, None)

    def add_layer(self, w, b):
        lr = NeuronLayer(len(self.layers) + 1,
                         w,
                         sigm=self.sigm,
                         sprime=self.default_sprime,
                         b=b,
                         l1=self.l1, l2=self.l2)
        self.layers.append(lr)

    def get_net_activation(self, x):  # a(nN)
        # x(n0)
        self.x = x
        for lr in self.layers:
            acts = lr.get_activation(x)
            x = acts
        return x

    def calc_out_error(self, y):  # y(nN)
        last = self.layers[-1]
        return last.calc_out_error(y)

    def backprop_error(self):
        next_lr = self.layers[-1]
        for i in range(len(self.layers) - 2, -1, -1):
            lr = self.layers[i]
            lr.backward_error(next_lr)
            next_lr = lr

    def update_weights(self, learning_rate: float):
        prev_a = self.x
        for lr in self.layers:
            lr.recalc_weights(learning_rate, prev_a)
            prev_a = lr.a

    def train_batch(self,
                    x_batch,
                    y_batch) -> List[List[float]]:
        # M - batch size, NL0 - neurons on 0 (input) layer
        # x_batch: (NL0, M)
        # y_batch: (NLn, M)
        # calculate error on all the samples, then back propagate error
        # returns: activation values, (NLn, M)
        self.set_zero_error()
        activations = []

        bat_sz = len(x_batch)
        for i in range(len(x_batch)):
            x = np.array(x_batch[i])
            y = np.array(y_batch[i])
            ys = self.get_net_activation(x)
            activations.append(ys)
            self.calc_out_error(y)
        self.backprop_error()

        for l in self.layers:
            l.error /= bat_sz
        self.recalc_weights(self.learning_rate)
        return activations

    def train(self,
              x_set,  # x_batch: (NL0, N)
              y_set,  # y_batch: (NLn, N)
              batch_sz: int = 0,
              max_gens: int = 100,  # max generations
              min_delta_j: float = 0):
        batch_sz = batch_sz or min(len(x_set), 10)
        indices = [i for i in range(len(x_set))]

        last_er = 0
        for genr in range(max_gens):
            # make batches
            bat_inds = random.sample(indices, batch_sz)
            x_batch = [x_set[i] for i in bat_inds]
            y_batch = [y_set[i] for i in bat_inds]
            # calc output, calc error grad and backprop error
            ys = self.train_batch(x_batch, y_batch)

            # calc error and check stop condition
            err = 0.0
            for i in range(len(y_batch)):
                y = np.array(y_batch[i])
                err += self.error_func(y, ys[i])
            err /= len(y_batch)
            self.error_on_trainset.append(err)
            if genr > 0 and min_delta_j and abs(err - last_er) < min_delta_j:
                return
            last_er = err

    def set_zero_error(self):
        for l in self.layers:
            l.error = 0.0

    def recalc_weights(self, learning_rate: float):
        prev_a = self.x
        for lr in self.layers:
            lr.recalc_weights(learning_rate, prev_a)
            prev_a = lr.a
