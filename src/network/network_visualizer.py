import plotly.express as px
import pandas as pd
from src.network.network import NeuronNetwork


class NetworkVisualizer:
    def build_learning_curve(self, network: NeuronNetwork):
        data = pd.DataFrame([(i, network.error_on_trainset[i])
                             for i in range(len(network.error_on_trainset))],
                            columns=['x', 'y'])
        fig = px.line(data, x="x", y="y", title='Error by gen')
        fig.show()