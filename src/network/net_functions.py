import math


# square error function
def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def sigmoid_prime(x):
    s = 1 / (1 + math.exp(-x))
    return s * (1 - s)


def error_fn_distance(y, ys) -> float:
    er = 0
    for i in range(len(y)):
        er += (y[i] - ys[i]) ** 2
    return er / len(y)


# cross-entropy error function
def error_cross_entropy(y, ys) -> float:
    er = 0
    for i in range(len(y)):
        er += y[i] * math.log(ys) + (1 - ys) * math.log(1 - ys)
    er /= -len(y)
    return er


def cross_entropy_prime(x):
    return 1
