from typing import List


class TokenizedText:
    def __init__(self,
                 pos_tokens: List[str] = None):
        self.pos_tokens = pos_tokens