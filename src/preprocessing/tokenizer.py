from typing import Generator

import nltk
from lexnlp.nlp.en.segments.sentences import get_sentence_span_list

from src.preprocessing.tokenized_text import TokenizedText


class Tokenizer:
    @staticmethod
    def split_on_sentences(text: str) -> Generator[str, None, None]:
        for span in get_sentence_span_list(text):
            yield text[span[0]:span[1]]

    def tokenize_text(self, text: str) -> TokenizedText:
        words = nltk.word_tokenize(text)
        pos_tokens = nltk.pos_tag(words)
        return TokenizedText(pos_tokens=pos_tokens)