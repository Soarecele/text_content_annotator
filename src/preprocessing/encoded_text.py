from typing import List


class EncodedText:
    def __init__(self,
                 word_nums: List[float] = None,
                 pos_nums: List[float] = None):
        self.word_nums = word_nums
        self.pos_nums = pos_nums