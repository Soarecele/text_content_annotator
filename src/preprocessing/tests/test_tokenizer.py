from unittest import TestCase

from src.preprocessing.tokenizer import Tokenizer


class TestTokenizer(TestCase):
    def test_tokenize_phrase(self):
        text = 'Once upon a midnight dreary, while I pounded (weak and weary), ' + \
               ' over many quiant an cuorious volume of forgotten lore...'
        tok = Tokenizer()

        import time
        delta = 0

        for i in range(100):
            start = time.time()
            vector = tok.tokenize_text(text)
            self.assertIsNotNone(vector)
            end = time.time()
            delta += end - start

        self.assertLess(delta / 100, 5)