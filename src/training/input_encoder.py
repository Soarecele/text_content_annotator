from typing import Dict, List, Tuple
import codecs
import json

from src.preprocessing.tokenizer import Tokenizer


class InputEncoder:
    def __init__(self):
        self.max_tokens = 120
        self.pos_bits = 0
        self.ant_bits = 0
        self.pos_token_code = {}  # type: Dict[str, int]
        self.annotation_code = {}  # type: Dict[str, int]
        self.tokenizer = Tokenizer()

    def initialize(self,
                   path_to_stat: str,
                   max_tokens: int = 0):
        self.max_tokens = max_tokens or self.max_tokens
        # read annotations and POS tokens from stat. file
        pos_tokens = []
        annotations = []
        collections = [None, pos_tokens, annotations, None]
        col_index = 0
        collection = collections[col_index]

        with codecs.open(path_to_stat, 'r', encoding='utf-8') as fr:
            for line in fr.readlines():
                if line.startswith('-- '):
                    continue  # comment
                if line.startswith('---------'):
                    col_index += 1
                    collection = collections[col_index]
                    continue
                if collection is None:
                    continue
                # split "$:2792" or "act:172" or "#colon#:9371"
                parts = line.split(':')
                if len(parts) != 2:
                    continue
                collection.append((parts[0], int(parts[1])))

        # build pos_token_codes
        # +1 for all POSs that may appear in non-training texts
        pos_count = len(pos_tokens) + 1
        self.pos_bits = pos_count

        ants_count = len(annotations)
        self.ant_bits = ants_count

        for i in range(len(pos_tokens)):
            self.pos_token_code[pos_tokens[i][0]] = 1 << i

        for i in range(len(annotations)):
            self.annotation_code[annotations[i][0]] = 1 << i

    def encode_sample(self,
                      word_tokens: List[Tuple[str, str]],
                      annotations: List[str]) -> Tuple[List[int], int]:
        # calculate inputs: each token is coded by one integer
        inputs = []
        for t in word_tokens[:self.max_tokens]:
            t_code = self.pos_token_code.get(t[1]) or len(self.pos_token_code)
            inputs.append(t_code)

        outp = 0
        for ant in annotations:
            mask = self.annotation_code[ant]
            outp = outp | mask

        return inputs, outp

    def serialize_settings(self, file_path: str):
        data = {
            'max_tokens': self.max_tokens,
            'pos_bits': self.pos_bits,
            'ant_bits': self.ant_bits,
            'pos_token_code': self.pos_token_code,
            'annotation_code': self.annotation_code
        }
        with codecs.open(file_path, 'w', encoding='utf-8') as fw:
            fw.write(json.dumps(data, indent=2))

    def deserialize_settings(self, file_path: str):
        with codecs.open(file_path, 'w', encoding='utf-8') as fr:
            str_json = fr.read()
        data = json.loads(str_json)[0]
        self.max_tokens = data['max_tokens']
        self.pos_bits = data['pos_bits']
        self.ant_bits = data['ant_bits']
        self.pos_token_code = data['pos_token_code']
        self.annotation_code = data['annotation_code']





