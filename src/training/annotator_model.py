import codecs
import math
from typing import List, Tuple, Callable

from keras.engine.saving import load_model

from src.training.input_encoder import InputEncoder
from src.training.source_file_parser import SourceFileParser
from keras import Sequential
from keras.layers import Dense
import numpy as np


class AnnotatorModel(SourceFileParser):
    def __init__(self):
        super().__init__()
        self.encoder = InputEncoder()
        self.x = []  # type:List[List[int]]
        self.y = []  # type:List[int]
        self.x_bits = []  # type:List[List[int]]
        self.y_bits = []  # type:List[List[int]]
        self.path_to_stat = ''
        self.model = None  # type:Sequential
        self.input_sz = 0
        self.output_sz = 0
        self.positive_threshold = 0.5

    def initialize(self,
                   path_to_stat: str,
                   max_tokens: int = 0):
        self.encoder.initialize(path_to_stat, max_tokens=max_tokens)
        self.path_to_stat = path_to_stat

    def load_vectors(self, parsed_text_path: str):
        self.process_src_file(parsed_text_path)

    def save_vectors(self, file_path: str):
        with codecs.open(file_path, 'w', encoding='utf-8') as fw:
            for i in range(len(self.x)):
                fw.write(','.join([str(x) for x in self.x[i]]))
                fw.write(';')
                fw.write(str(self.y[i]))
                fw.write('\n')

    def load_vectors_from_path(self,
                               file_path: str,
                               skip_function: Callable[[int], bool] = None):
        i = 0
        with codecs.open(file_path, 'r', encoding='utf-8') as fr:
            for line in fr.readlines():
                if not line:
                    continue
                i += 1
                if skip_function is not None and skip_function(i):
                    continue
                parts = line.split(';')
                x = [int(p) for p in parts[0].split(',')]
                y = int(parts[1])
                self.x.append(x)
                self.y.append(y)

    def train(self,
              batch_size: int = 10,
              max_gens: int = 500,
              verbose: int = 1):
        """
        :param batch_size: SGD NN optimizer batch size
        :param max_gens: max generations (iterations) for optimizations
        :param verbose: log (1) or don't log training progress
        :return:
        """
        if not self.model:
            self.create_model()
        self.unpack_bits()
        self.model.fit(np.array(self.x_bits).astype("float32"),
                       np.array(self.y_bits).astype("float32"),
                       epochs=max_gens,
                       batch_size=batch_size,
                       verbose=verbose,
                       validation_split=0.12)

    def create_model(self):
        self.model = Sequential()

        self.input_sz = self.encoder.pos_bits * self.encoder.max_tokens
        self.output_sz = self.encoder.ant_bits
        inter_count = 2  # count of inner layers

        inter_layers = []
        ln_0 = math.log10(self.input_sz)
        ln_n = math.log10(self.output_sz)
        total_layers = inter_count + 2
        for i in range(1, inter_count + 1):
            pw = ln_0 * (1 - i / total_layers) + ln_n * i / total_layers
            count = int(math.ceil(math.pow(10, pw)))
            inter_layers.append(count)

        self.model.add(Dense(self.input_sz,
                       input_dim=self.input_sz, activation='elu'))
        for il in inter_layers:
            self.model.add(Dense(il, activation='elu'))
        self.model.add(Dense(self.output_sz, activation='sigmoid'))

        self.model.compile(loss="binary_crossentropy",
                           optimizer="adam",
                           metrics=['accuracy'])

    def save_model(self, path: str):
        self.model.save(path)

    def load_model(self, path: str):
        self.create_model()
        self.model = load_model(path)  # .load_weights(path)

    def check_on_train_data(self):
        self.unpack_bits()

        ys = self.model.predict(np.array(self.x_bits))
        y_bit_len = self.encoder.ant_bits

        positives = [0] * y_bit_len
        negatives = [0] * y_bit_len
        fal_pos = [0] * y_bit_len
        fal_neg = [0] * y_bit_len

        for i in range(len(ys)):
            yi = self.y_bits[i]
            ysi = [1 if y > self.positive_threshold else 0 for y in ys[i]]
            for j in range(y_bit_len):
                if yi[j] and ysi[j]:
                    positives[j] = positives[j] + 1
                if not yi[j] and not ysi[j]:
                    negatives[j] = negatives[j] + 1
                if yi[j] and not ysi[j]:
                    fal_neg[j] = fal_neg[j] + 1
                if not yi[j] and ysi[j]:
                    fal_pos[j] = fal_pos[j] + 1

        # "decipher" results
        codes = [(self.encoder.annotation_code[c], c) for c in self.encoder.annotation_code]
        codes.sort(key=lambda c: c[0])

        # Annotation - pos - neg - FP - FN
        results_summary = []  # List[Tuple[str, int, int, int, int]]
        for i in range(len(positives)):
            results_summary.append((codes[i][1], positives[i],  # negatives[i],
                                   fal_pos[i], fal_neg[i]))
        total_records = len(ys)

    def unpack_bits(self):
        """
        :param batch_size: SGD NN optimizer batch size
        :param max_gens: max generations (iterations) for optimizations
        :return:
        """
        # "unzip" data from ints to bits
        self.x_bits = []
        self.y_bits = []
        for i in range(len(self.x)):
            x_line = [0] * self.input_sz
            shift = 0
            for x in self.x[i]:
                if x:
                    x_shift = int(math.log(x, 2))
                    x_line[shift + x_shift] = 1
                shift += self.encoder.pos_bits
            self.x_bits.append(x_line)

            y = self.y[i]
            y_line = [0] * self.output_sz
            for j in range(self.output_sz):
                if y & (1 << j):
                    y_line[j] = 1
            self.y_bits.append(y_line)

    def on_line_processed(self,
                          sentence: str,
                          tok_text: List[Tuple[str, str]],
                          annotations: List[str]):
        x, y = self.encoder.encode_sample(tok_text, annotations)
        self.x.append(x)
        self.y.append(y)


