import os
from unittest import TestCase

from src.training.annotator_model import AnnotatorModel


class TestPhrasePosTokens(TestCase):
    own_folder = os.path.abspath(os.path.dirname(__file__) + '/../../')
    src_path = os.path.join(own_folder, 'test_data/proc_data/en_parsed.txt')
    stat_path = os.path.join(own_folder, 'training/data/statistics/en.txt')
    vectors_path = os.path.join(own_folder, 'training/data/vectors/en_vectors.txt')
    network_path = os.path.join(own_folder, 'training/data/network/en_network.txt')

    def test_load(self):
        model = AnnotatorModel()
        model.initialize(self.stat_path)
        model.load_vectors(self.src_path)
        model.save_vectors(self.vectors_path)

    def test_train(self):
        model = AnnotatorModel()
        model.initialize(self.stat_path)
        # 1/8 of the data is skipped for future test
        model.load_vectors_from_path(self.vectors_path,
                                     skip_function=lambda i: i % 8 == 0)
        model.train(batch_size=20,
                    max_gens=100,
                    verbose=1)
        model.save_model(self.network_path)

    def test_estimate(self):
        model = AnnotatorModel()
        model.initialize(self.stat_path)
        model.load_model(self.network_path)
        # 1/8 of the data is skipped for future test
        model.load_vectors_from_path(self.vectors_path,
                                     skip_function=lambda i: i % 8 != 0)
        model.check_on_train_data()

