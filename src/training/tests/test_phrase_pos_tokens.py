import os
from unittest import TestCase

from src.training.explore_phrases_pos_tokens import PhrasesPosTokensExplorer


class TestPhrasePosTokens(TestCase):
    def test_build_stat(self):
        own_folder = os.path.abspath(os.path.dirname(__file__) + '/../../')
        src_path = os.path.join(own_folder, 'test_data/proc_data/en_parsed.txt')
        out_path = os.path.join(own_folder, 'training/data/statistics/en.txt')

        exp = PhrasesPosTokensExplorer()
        exp.collect_statistics(src_path)
        exp.save_stat_in_file(out_path)
