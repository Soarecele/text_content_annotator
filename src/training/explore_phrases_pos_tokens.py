from typing import Dict, List, Tuple
import codecs

from src.training.source_file_parser import SourceFileParser


class PhrasesPosTokensExplorer(SourceFileParser):
    def __init__(self):
        super().__init__()
        self.phrase_len_count = {}  # type: Dict[int, int]
        self.pos_token_count = {}  # type: Dict[str, int]
        self.annotations_count = {}  # type: Dict[str, int]

    def collect_statistics(self, proc_text_file_path: str):
        self.process_src_file(proc_text_file_path)

    def save_stat_in_file(self, out_path: str):
        with codecs.open(out_path, 'w', encoding='utf-8') as fw:
            # write phrases stat
            fw.write(f'-- phrase len:\n')
            phrase_lens = [(p, self.phrase_len_count[p])
                           for p in self.phrase_len_count]
            phrase_lens.sort(key=lambda p: p[0])
            phrase_lens_str = '\n'.join([f'{p[0]}:{p[1]}' for p in phrase_lens])
            fw.write(phrase_lens_str)
            fw.write('\n' + self.text_separator + '\n')

            # write POS stat
            fw.write(f'-- POS count:\n')
            pos_count = [(p, self.pos_token_count[p])
                         for p in self.pos_token_count]
            pos_count.sort(key=lambda p: p[0])
            # escape POS tokens
            pos_count = [('#colon#' if p[0] == ':' else p[0], p[1]) for p in pos_count]
            pos_count_str = '\n'.join([f'{p[0]}:{p[1]}' for p in pos_count])
            fw.write(pos_count_str)
            fw.write('\n' + self.text_separator + '\n')

            # write ants stat
            fw.write(f'-- annotations count:\n')
            ants_ct = [(p, self.annotations_count[p])
                       for p in self.annotations_count]
            ants_ct.sort(key=lambda p: p[0])
            ants_ct_str = '\n'.join([f'{p[0]}:{p[1]}' for p in ants_ct])
            fw.write(ants_ct_str)

    def on_line_processed(self,
                          sentence: str,
                          tok_text: List[Tuple[str, str]],
                          annotations: List[str]):
        for _, pos in tok_text:
            count = self.pos_token_count.get(pos[1]) or 0
            self.pos_token_count[pos] = count + 1

        ln = len(tok_text)
        old_ln = self.phrase_len_count.get(ln) or 0
        self.phrase_len_count[ln] = old_ln + 1

        for ant in annotations:
            old_ct = self.annotations_count.get(ant) or 0
            self.annotations_count[ant] = old_ct + 1
