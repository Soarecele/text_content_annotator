from typing import List
import codecs

from src.preprocessing.tokenized_text import TokenizedText
from src.preprocessing.tokenizer import Tokenizer


class SourceFileParser:
    text_separator = '--------------------------------------------------------'

    def __init__(self):
        self.tokenizer = Tokenizer()
        self.file_parts = ['sent', 'ants']
        self.part_index = 0
        self.last_sent = ''

    def on_line_processed(self,
                          sentence: str,
                          tok_text: TokenizedText,
                          annotations: List[str]):
        raise NotImplementedError()

    def process_src_file(self, proc_text_file_path: str):
        with codecs.open(proc_text_file_path, 'r', encoding='utf-8') as fr:
            line = fr.readline()
            while line:
                self.process_file_line(line)
                line = fr.readline()

    def process_file_line(self, line: str):
        if not line:
            return
        line = line.rstrip('\n')
        if line == self.text_separator:
            self.part_index += 1
            if self.part_index == len(self.file_parts):
                self.part_index = 0
            return

        part = self.file_parts[self.part_index]
        if part == 'sent':
            self.last_sent += (line or '\n')
            return

        pos_list = self.tokenizer.tokenize_text(self.last_sent).pos_tokens
        # parse "annotations: amount,date,definition"
        ants = line.strip()[len('annotations: '):].split(',')
        ants = [a for a in ants if a]
        self.on_line_processed(self.last_sent, pos_list, ants)

        self.last_sent = ''
