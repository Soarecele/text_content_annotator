import codecs
from typing import Dict, Callable, Generator
import os

from lexnlp.extract.en.acts import get_acts
from lexnlp.extract.en.amounts import get_amounts
from lexnlp.extract.en.citations import get_citations
from lexnlp.extract.en.conditions import get_conditions
from lexnlp.extract.en.constraints import get_constraints
from lexnlp.extract.en.copyright import get_copyright
from lexnlp.extract.en.courts import _get_courts
from lexnlp.extract.en.cusip import get_cusip
from lexnlp.extract.en.dates import get_dates
from lexnlp.extract.en.definitions import get_definitions
from lexnlp.extract.en.distances import get_distances
from lexnlp.extract.en.durations import get_durations
from lexnlp.extract.en.money import get_money
from lexnlp.extract.en.percents import get_percents
from lexnlp.extract.en.pii import get_pii
from lexnlp.extract.en.ratios import get_ratios
from lexnlp.extract.en.regulations import get_regulations
from lexnlp.extract.en.trademarks import get_trademarks
from lexnlp.extract.en.urls import get_urls

from src.preprocessing.tokenizer import Tokenizer


class RawDataProcessor:

    text_separator = '--------------------------------------------------------'
    text_subst = '--------------------------------------'

    def __init__(self):
        self.annotators = {}  # type:Dict[str, Callable]

    def process_files(self, src_folder: str, dest_file_path: str):
        files = list(self.get_files_in_dir(src_folder))
        with codecs.open(dest_file_path, 'w', encoding='utf-8') as fw:
            for i in range(len(files)):
                print(f'Processing {i + 1} of {len(files)}')
                self.process_file(files[i], fw)
        print('Completed')

    @staticmethod
    def get_files_in_dir(folder: str) -> Generator[str, None, None]:
        for path in os.listdir(folder):
            file_path =  os.path.join(folder, path)
            if os.path.isfile(file_path):
                yield file_path

    def process_file(self, src_path: str, target_stream):
        with codecs.open(src_path, 'r', encoding='utf-8') as fr:
            text = fr.read()
        text = text.replace(RawDataProcessor.text_separator,
                            RawDataProcessor.text_subst)
        for sent in Tokenizer.split_on_sentences(text):
            # find all annotations
            ant_count = {a: self.annotators[a](sent) for a in self.annotators}
            ants_str = 'annotations: ' + ','.join([a for a in ant_count if ant_count[a] > 0])
            target_stream.write(sent + '\n')
            target_stream.write(RawDataProcessor.text_separator + '\n')
            target_stream.write(ants_str + '\n')
            target_stream.write(RawDataProcessor.text_separator + '\n')


en_processor = RawDataProcessor()
en_processor.annotators['amount'] = lambda s: len(list(get_amounts(s)))
en_processor.annotators['act'] = lambda s: len(list(get_acts(s)))
en_processor.annotators['citation'] = lambda s: len(list(get_citations(s)))
en_processor.annotators['condition'] = lambda s: len(list(get_conditions(s)))
en_processor.annotators['constraint'] = lambda s: len(list(get_constraints(s)))
en_processor.annotators['copyright'] = lambda s: len(list(get_copyright(s)))
en_processor.annotators['court'] = lambda s: len(list(_get_courts(s)))
en_processor.annotators['cusip'] = lambda s: len(list(get_cusip(s)))
en_processor.annotators['date'] = lambda s: len(list(get_dates(s)))
en_processor.annotators['definition'] = lambda s: len(list(get_definitions(s)))
en_processor.annotators['distance'] = lambda s: len(list(get_distances(s)))
en_processor.annotators['duration'] = lambda s: len(list(get_durations(s)))
en_processor.annotators['money'] = lambda s: len(list(get_money(s)))
en_processor.annotators['percent'] = lambda s: len(list(get_percents(s)))
en_processor.annotators['pii'] = lambda s: len(list(get_pii(s)))
en_processor.annotators['ratio'] = lambda s: len(list(get_ratios(s)))
en_processor.annotators['regulation'] = lambda s: len(list(get_regulations(s)))
en_processor.annotators['trademark'] = lambda s: len(list(get_trademarks(s)))
en_processor.annotators['url'] = lambda s: len(list(get_urls(s)))
