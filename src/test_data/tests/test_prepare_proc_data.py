import os
from unittest import TestCase

from src.test_data.process_raw_data import en_processor


class TestPrepareProcData(TestCase):
    def test_process_all_locales(self):
        locale_parser = {'en': en_processor}
        own_folder = os.path.abspath(os.path.dirname(__file__) + '/../')
        raw_folder = os.path.join(own_folder, 'raw_data')
        dest_folder = os.path.join(own_folder, 'proc_data')

        for dir_name in os.listdir(raw_folder):
            full_dir_path = os.path.join(raw_folder, dir_name)
            if not os.path.isdir(full_dir_path):
                continue

            pars = locale_parser.get(dir_name)
            if not pars:
                continue
            out_path = os.path.join(dest_folder, f'{dir_name}_parsed2.txt')
            pars.process_files(full_dir_path, out_path)
